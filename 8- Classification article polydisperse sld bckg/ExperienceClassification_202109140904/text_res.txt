\begin{itemize}\item - threshold\item - contrast_normalization\item - log\item - standard_scaler\itemCNN 1D transformer\begin{itemize} 
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Max Pooling (size: 4)
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Max Pooling (size: 4)
\item Flatten layer
\end{itemize} 
\item 
 {'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': 'balanced', 'criterion': 'gini', 'max_depth': None, 'max_features': 'auto', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 200, 'n_jobs': 5, 'oob_score': False, 'random_state': None, 'verbose': 2, 'warm_start': False}\end{itemize}: 93.5% 
\begin{itemize}\item - threshold\item - contrast_normalization\item - log\item - standard_scaler\item - reshape\itemCNN 1D Classifier 
egin{itemize} 
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Flatten layer
\item Fully connected: (activation: softmax)
\end{itemize} 
\end{itemize}: 90.4% 
\begin{itemize}\item - threshold\item - log\item - I0\item - standard_scaler\item - reshape\itemCNN 1D Classifier 
egin{itemize} 
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Flatten layer
\item Fully connected: (activation: softmax)
\end{itemize} 
\end{itemize}: 90.3% 
\begin{itemize}\item - threshold\item - contrast_normalization\item - log\item - I0\item - standard_scaler\item - reshape\itemCNN 1D Classifier 
egin{itemize} 
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Flatten layer
\item Fully connected: (activation: softmax)
\end{itemize} 
\end{itemize}: 88.1% 
\begin{itemize}\item - threshold\item - log\item - standard_scaler\item - reshape\itemCNN 1D Classifier 
egin{itemize} 
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Conv 1D (n filters: 32, kernel size: 10, activation: relu
\item Conv 1D (n filters: 16, kernel size: 10, activation: relu
\item Max Pooling (size: (4,))
\item Flatten layer
\item Fully connected: (activation: softmax)
\end{itemize} 
\end{itemize}: 87.8% 
\begin{itemize}\item - threshold\item - contrast_normalization\item - I0\item - log\item 
 {'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': 'balanced', 'criterion': 'gini', 'max_depth': None, 'max_features': 'auto', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 200, 'n_jobs': 5, 'oob_score': False, 'random_state': None, 'verbose': 2, 'warm_start': False}\end{itemize}: 79.2% 
\begin{itemize}\item - threshold\item - I0\item - log\item 
 {'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': 'balanced', 'criterion': 'gini', 'max_depth': None, 'max_features': 'auto', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 200, 'n_jobs': 5, 'oob_score': False, 'random_state': None, 'verbose': 2, 'warm_start': False}\end{itemize}: 79.2% 
\begin{itemize}\item - threshold\item - I0\item - log\item 
 {'algorithm': 'auto', 'leaf_size': 30, 'metric': 'minkowski', 'metric_params': None, 'n_jobs': 5, 'n_neighbors': 1, 'p': 2, 'weights': 'uniform'}\end{itemize}: 78.3% 
\begin{itemize}\item - threshold\item - contrast_normalization\item - I0\item - log\item 
 {'algorithm': 'auto', 'leaf_size': 30, 'metric': 'minkowski', 'metric_params': None, 'n_jobs': 5, 'n_neighbors': 1, 'p': 2, 'weights': 'uniform'}\end{itemize}: 78.3% 
\begin{itemize}\item - threshold\item - contrast_normalization\item - standard_scaler\item 
 {'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': 'balanced', 'criterion': 'gini', 'max_depth': None, 'max_features': 'auto', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 200, 'n_jobs': 5, 'oob_score': False, 'random_state': None, 'verbose': 2, 'warm_start': False}\end{itemize}: 76.4% 
\begin{itemize}\item - threshold\item - log\item 
 {'bootstrap': True, 'ccp_alpha': 0.0, 'class_weight': 'balanced', 'criterion': 'gini', 'max_depth': None, 'max_features': 'auto', 'max_leaf_nodes': None, 'max_samples': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 200, 'n_jobs': 5, 'oob_score': False, 'random_state': None, 'verbose': 2, 'warm_start': False}\end{itemize}: 66.6% 
\begin{itemize}\item - threshold\item - log\item 
 {'algorithm': 'auto', 'leaf_size': 30, 'metric': 'minkowski', 'metric_params': None, 'n_jobs': 5, 'n_neighbors': 1, 'p': 2, 'weights': 'uniform'}\end{itemize}: 41.2% 

# -*- coding: utf-8 -*-
import xsact.wrapper as xenocs
import numpy as np
import sys
from copy import copy
import xsact.deps_readData as deps_readData
from xsact.deps_precision import unit_conversion
import onnxruntime as rt
import time
import pandas as pd

# Preprocessing function take somes arguments:
# - X: ndarray of dimension n_samples * n_pts * 2. First X[:,:,0] correspond to intensity, X[:,:,1] is the q vector
# - *arg: other argument to pass to the functions.

def set_qmin(X, minimal_q_min):
    """ Remove first points if qmin to small and adapt length using interpolation """

    X_copy = copy(X)  # copy X for first preprocessing to conserve original data
    n_pts = X_copy.shape[1]
    for i in range(X_copy.shape[0]):
        if X_copy[i, :, 1].min() < minimal_q_min:
            pass
        else:
            new_q = np.linspace(minimal_q_min, X_copy[i, -1, 1], n_pts)
            new_I = np.interp(new_q, X_copy[i, :, 1], X_copy[i, :, 0])
            X_copy[i, :, 0] = new_I
            X_copy[i, :, 1] = new_q
    return X_copy


def log(X, threshold=-10):
    X[:, :, 0] = np.log(X[:, :, 0])
    X[:, :, 0][X[:, :, 0] < threshold] = threshold
    return X


def threshold(X, thres=1e-15):
    X[:, :, 0][X[:, :, 0] < thres] = thres
    return X


def contrast_normalization(X):
    """
    X[i,:,0]/sum(X[i,:,1]*(X[i,:,2]**2))
    """

    def f(x_slice, q):
        return x_slice / sum(x_slice * (q ** 2))

    for i in range(X.shape[0]):
        X[i, :, 0] = f(X[i, :, 0], X[i, :, 1])
    return X


def log_q_scaling(X):
    def get_new_I_q(Xi):  # one I and q vector
        I = Xi[:, 0]
        q = Xi[:, 1]
        log_q = np.log(q)
        max_log_q = log_q.max()
        min_log_q = log_q.min()
        log_q_sampling = np.exp(np.linspace(min_log_q, max_log_q, len(q)))

        Xi[:, 0] = np.interp(log_q_sampling, q, I)  # new I values
        Xi[:, 1] = log_q_sampling  # new q values
        return Xi

    for i in range(X.shape[0]):
        X[i] = get_new_I_q(X[i])
    return X


def standardization(X, mean_ref, std_ref, q_ref):
    """
    Standardization using a prefitted values of mean and std. mean_ref, std_ref are the mean and std vectors corresponding to
    q_ref values.
    """
    for i in range(X.shape[0]):
        mean = np.interp(X[i, :, 1], q_ref, mean_ref)
        std = np.interp(X[i, :, 1], q_ref, std_ref)
        X[i, :, 0] = (X[i, :, 0] - mean) / std
    return X


def apply_preprocess(X, functions, ):
    """
    Apply preprocess without sklearn transformers and pipeline
    """

    for fun, kwargs in functions:
        X = fun(X, **kwargs)
    return X





def do_onnx_prediction(X, transformer_path, classifier_path):
    """
    Arguments:
    - X: 2 or 3D numpy array. If 2D, first dimension correspond the intensity vector, second is the q vector. If 3D,
    first dimension is the number of sample dimension.
    - transformer_path: path to the ONNX file of the transformer.
    - classifier_path: path to the ONNX file of the classifier.

    Return classifier prediction as a list of two elements:
    - first element is an array with the predicted labels
    - second element is a list of dict. Each dict take the labels as key and probabilities as values.

    Ex of output:
    [array([6], dtype=int64),
     [{0: 0.04172930866479874,
       1: 0.11926474422216415,
       2: 0.04086703434586525,
       3: 0.04815244674682617,
       4: 0.04331471398472786,
       5: 0.12147034704685211,
       6: 0.42928871512413025,
       7: 0.04533030465245247,
       8: 0.1105823814868927},]
    """

    if len(X.shape) == 2:
        X = X[None, :, :]

    # sess = rt.InferenceSession("/home/nicolas/Documents/xai/xai/notebooks_nico/22- ONNX test/models/onnx_models/transformer.onnx", providers=rt.get_available_providers()[1:])
    sess = rt.InferenceSession(transformer_path, providers=rt.get_available_providers()[1:])

    # Remove TF provider because it raise warning (missing some requirements) in InferenceSession and make an error when do run function.
    input_name = sess.get_inputs()[0].name
    input_shape = sess.get_inputs()[0].shape
    input_type = sess.get_inputs()[0].type

    output_name = sess.get_outputs()[0].name
    output_shape = sess.get_outputs()[0].shape
    output_type = sess.get_outputs()[0].type

    input_shape = X.shape
    embedings = sess.run([output_name],
                         {input_name: np.array(X, dtype='float32').reshape(input_shape[0], input_shape[1], -1)})

    # sess_xgb = rt.InferenceSession("/home/nicolas/Documents/xai/xai/notebooks_nico/22- ONNX test/models/onnx_models/pipeline_xgboost.onnx", providers=rt.get_available_providers()[1:])
    sess = rt.InferenceSession(classifier_path, providers=rt.get_available_providers()[1:])
    input_name = sess.get_inputs()[0].name
    output_name = sess.get_outputs()[1].name
    pred_onnx = sess.run(None, {input_name: embedings[0].astype(np.float32)})

    # return embedings[0], pred_onnx
    return pred_onnx


class FctObject(xenocs.ModuleFctObject):
    def do_my_function(
            self,
            indata,
    ):
        start_time = time.time()

        # Read data
        [q, I, sig], qunit, unitI = deps_readData.read_check_I_of_q(self, indata, mask_neg=False, ignoreNaN=False,
                                                                    extramsg="Input data :")
        q = unit_conversion(q, qunit, "m-1")
        I = unit_conversion(I, unitI.value, "cm-1")
        sig = unit_conversion(sig, unitI.value, "cm-1")
        print("Read data --- %s seconds ---" % (time.time() - start_time))

        start_time = time.time()

        # Format input
        X = np.array([I, q]).reshape(-1, 2)
        X = X[None, :,
            :]  # add new first dimension (it is the number of samples dimension, which is 1 in the case of single data prediction)

        # Load standardization parameters
        saving_path = 'path/to/standardization/directory/'  # adapt path
        standardization_mean = pd.read_csv(saving_path + 'mean_std_scaler.csv', index_col=0).squeeze("columns")
        standardization_var = pd.read_csv(saving_path + 'std_std_scaler.csv', index_col=0).squeeze("columns")
        standardization_q_ref = pd.read_csv(saving_path + 'q_ref_std_scaler.csv', index_col=0).squeeze("columns")

        # ONNX transformer
        transformer_path = 'path/to/transformer.onnx'

        # ONNX classifier
        classifier_path = 'path/to/classifier.onnx'

        # Define and apply preprocessing pipeline
        functions = [
            (set_qmin, {'minimal_q_min': 0.005}),
            (threshold, {'thres': 1e-15}),
            (contrast_normalization, {}),
            (log, {'threshold': -10}),
            (standardization, {'mean_ref': standardization_mean.values,
                               'std_ref': standardization_var.values,
                               'q_ref': standardization_q_ref.values}),
            (log_q_scaling, {}), ]

        X = apply_preprocess(X, functions)
        prediction = do_onnx_prediction(X, transformer_path, classifier_path)

        label = prediction[0][0]
        predicted_proba = prediction[1]  # dict with labels number as keys and proba as values.

        # Todo: format output

        return results


def main():
    xenocs.handle(sys.argv, "Classification", "classification.xml", FctObject())


if __name__ == "__main__":


    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))

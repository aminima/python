Experience 202106180808:
Paramètres variables :
- Sphere:
	Radius: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Sld: uniforme [5, 131].10e6 A-2
	Bckg: uniforme [0, 0.3]
- Ellipsoid:
	Radius equat: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Coeff Radius polar: uniforme [0.1, 0.9] + poly gaussien (fwhm uniforme [0, 30]%)
	Sld: uniforme [5, 131].10e6 A-2
	Bckg: uniforme [0, 0.3]
- Cylinder:
	Radius: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Length coeff: uniforme [100, 200] (x radius) + poly gaussien (fwhm uniforme [0, 30]%)
	Sld: uniforme [5, 131].10e6 A-2
	Bckg: uniforme [0, 0.3]
- Core Shell Sphere:
	Radius: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Shell thickness: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Sld core: uniforme [5, 131].10e6 A-2
	Sld shell: uniforme [5, 131].10e6 A-2 except [0.9sld_core; 1.1sld_core]
	Bckg: uniforme [0, 0.3]
- Core Shell Ellipsoid:
	Radius equat: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Coeff Radius polar: uniforme [0.1, 0.9] + poly gaussien (fwhm uniforme [0, 30]%)
	Shell thickness: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Sld core: uniforme [5, 131].10e6 A-2
	Sld shell: uniforme [5, 131].10e6 A-2 except [0.9sld_core; 1.1sld_core]
	Bckg: uniforme [0, 0.3]
- Core Shell Cylinder:
	Radius: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Length coeff: uniforme [100, 200] (x radius) + poly gaussien (fwhm uniforme [0, 30]%)
	Shell thickness: uniforme [50, 2500]A + poly gaussien (fwhm uniforme [0, 30]%)
	Sld core: uniforme [5, 131].10e6 A-2
	Sld shell: uniforme [5, 131].10e6 A-2 except [0.9sld_core; 1.1sld_core]
	Bckg: uniforme [0, 0.3]
	
Paramètres statiques:
- volfr: 0.0073

Nombre simu : 17706 (2951x6)
